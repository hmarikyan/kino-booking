<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Films';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="films-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Films', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>"Room",
                'value' => function ($data) {
                    return $data->room->name;
                },
            ],
            'name',
            'author',
            'year',
            [
                'attribute'=>"Show Times",
                'format'=>'html',
                'value' => function ($data) {
                    $html = "";
                    if(!empty($data->filmTimes)){
                        foreach($data->filmTimes as $time){
                            $html .= $time['time']."<br>";
                        }
                    }

                    return $html;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
