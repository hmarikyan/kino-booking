<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Films */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="films-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'room_id')->dropDownList(
            ArrayHelper::map($rooms, 'id', 'name'),           // Flat array ('id'=>'label')
            ['prompt'=>'Select Room']    // options
    ); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>


    <div class="times form-group">
        <label class="control-label" for="">Show Times</label>
    <?php if(!empty($modelTimes)): ?>
        <?php foreach($modelTimes as $k=>$modelTime): ?>
            <?php if(!empty($modelTime['id'])): ?>
               <input type="hidden" class="form-control" name="FilmTimes[<?=$k?>][id]" value="<?=$modelTime['id']?>" >
            <?php endif ?>

            <input type="text" class="datetimepicker form-control input-ms" name="FilmTimes[<?=$k?>][time]" value="<?=$modelTime['time']?>" readonly="readonly">

        <?php endforeach ?>
    <?php endif ?>
    </div>

    <div class="form-group">
        <?= Html::button('Add time!', ['class' => 'add_time btn btn-success','data-id'=>isset($k)?$k:0]) ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
