<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
/* @var $this yii\web\View */

$this->title = 'Kino';
?>
<div class="site-index">

    <div class="row">
        <div class="alert-success sits-title">Room / Film / Time</div>
    </div>
    <div class="row"> <?php  $items = array();
        if(!empty($rooms)){
           foreach($rooms as $room){
              $one_room = array(
                  'label' => $room['name'],
                  'url' => '#',
                  'data-room_id' => $room['id']
              );

              if(!empty($room->films)){
                  $one_room['items'] = array();
                  foreach($room->films as $film){
                      $one_film = array(
                          'label' => $film['name'],
                          'url' => '#'
                      );

                      if(!empty($film->filmTimes)){
                          $one_film['items'] = array();
                          foreach($film->filmTimes as $time){
                              $one_film['items'][] = array(
                                  'label' => $time['time'],
                                  'url' => '#film='.$film->id."&time=".$time->id,
                                  'options'=>array(
                                      'class'=>'select_time',
                                      'data-film' => $film->id,
                                      'data-time' => $time->id
                                  )
                              );
                          }
                      }

                      $one_room['items'][] = $one_film;
                  }
              }

              $items[] = $one_room;
           }

//            print_r($items);
            echo Nav::widget([
                'items' => $items,
                'options' => ['class' =>'navbar-nav'], // set this to nav-tab to get tab-styled navigation
            ]);
        }else{
            echo "There is no any room!";
        } ?>
    </div>

    <div class="row sits-outer">
        <div class="alert-success sits-title">Sits <span id="film_time"></span></div>

        <table class="text-center table table-bordered table-hover table-striped sits">
            <?php for($row = 0; $row<10; $row++): ?>
            <tr data-row="<?=$row?>">
                <?php for($col = 0; $col< 10; $col++): ?>
                <td >
                    <button data-row="<?=$row ?>" data-col="<?=$col ?>" id="<?=$row."_".$col ?>" class="btn btn-primary btn_book"><?=$row+1 ?> / <?=$col+1 ?></button>
                </td>
                <?php endfor ?>
            </tr>
            <?php endfor ?>
        </table>

        <!-- Dialog for input booker name-->
        <div id="dialog-form" title="Booker">
            <p class="validateTips">Input your name.</p>

            <div id="dialog_error"></div>
            <div class="form-group">
                <input type="text" name="booker" id="booker" value="" class="form-control">
            </div>
        </div>
        <!-- end Dialog for input booker name-->
    </div>

</div>
