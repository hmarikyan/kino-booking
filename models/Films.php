<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "films".
 *
 * @property integer $id
 * @property integer $room_id
 * @property string $name
 * @property string $author
 * @property string $year
 *
 * @property Bookings[] $bookings
 * @property FilmTimes[] $filmTimes
 * @property Rooms $room
 */
class Films extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'films';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id'], 'required'],
            [['room_id'], 'integer'],
            [['year'], 'safe'],
            [['name', 'author'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'name' => 'Name',
            'author' => 'Author',
            'year' => 'Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Bookings::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmTimes()
    {
        return $this->hasMany(FilmTimes::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Rooms::className(), ['id' => 'room_id']);
    }

    /**
     * @inheritdoc
     * @return FilmsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilmsQuery(get_called_class());
    }
}
