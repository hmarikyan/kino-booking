<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookings".
 *
 * @property integer $id
 * @property integer $film_id
 * @property integer $film_time_id
 * @property integer $row
 * @property integer $column
 * @property string $booker
 *
 * @property FilmTimes $filmTime
 * @property Films $film
 */
class Bookings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['film_id', 'film_time_id', 'row', 'column'], 'integer'],
            [['booker'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Film ID',
            'film_time_id' => 'Film Time ID',
            'row' => 'Row',
            'column' => 'Column',
            'booker' => 'Booker',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmTime()
    {
        return $this->hasOne(FilmTimes::className(), ['id' => 'film_time_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    /**
     * @inheritdoc
     * @return BookingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookingsQuery(get_called_class());
    }
}
