<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "film_times".
 *
 * @property integer $id
 * @property integer $film_id
 * @property string $tme
 *
 * @property Bookings[] $bookings
 * @property Films $film
 */
class FilmTimes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'film_times';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['film_id'], 'integer'],
            [['time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Film ID',
            'time' => 'Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Bookings::className(), ['film_time_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    /**
     * @inheritdoc
     * @return FilmTimesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilmTimesQuery(get_called_class());
    }
}
