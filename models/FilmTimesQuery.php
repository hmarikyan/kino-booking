<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FilmTimes]].
 *
 * @see FilmTimes
 */
class FilmTimesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FilmTimes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FilmTimes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}