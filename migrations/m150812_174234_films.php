<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_174234_films extends Migration
{
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `films`");

        $this->execute("CREATE TABLE `films` (
                      `id` INT NOT NULL AUTO_INCREMENT COMMENT 'primary key',
                      `room_id` INT NOT NULL COMMENT 'foreign key - reference to rooms table',
                      `name` VARCHAR(50) NULL COMMENT '',
                      `author` VARCHAR(50) NULL COMMENT '',
                      `year` YEAR NULL COMMENT 'year of production',
                      PRIMARY KEY (`id`)  COMMENT '')
                      ");

        $this->execute("
          INSERT INTO `films` (`id`, `room_id`, `name`, `author`, `year`) VALUES
            (1, 3, 'Supernatural 2', 'Din & Sam', 2010),
            (2, 1, 'Furious 2', 'Fury', 2002),
            (3, 1, 'Terminator 3', 'Term', 2005),
            (5, 1, 'Titanik', 'Tito', 1995),
            (6, 2, 'Believe', 'Belo', 2012);
        ");

        echo __CLASS__." migrated.\n";
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `films`");

        echo __CLASS__." reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
