<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_182335_add_foreign_keys extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookings`
            ADD INDEX `fk_bookings_films1_idx` (`film_id` ASC)  COMMENT '',
            ADD INDEX `fk_bookings_film_times1_idx` (`film_time_id` ASC)  COMMENT '';

            ALTER TABLE `film_times`
            ADD INDEX `fk_film_times_films1_idx` (`film_id` ASC)  COMMENT '';

            ALTER TABLE `films`
            ADD INDEX `fk_films_rooms_idx` (`room_id` ASC)  COMMENT '';

            ALTER TABLE `bookings`
            ADD CONSTRAINT `fk_bookings_films1`
              FOREIGN KEY (`film_id`)
              REFERENCES `films` (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE,
            ADD CONSTRAINT `fk_bookings_film_times1`
              FOREIGN KEY (`film_time_id`)
              REFERENCES `film_times` (`id`)
              ON DELETE SET NULL
              ON UPDATE CASCADE;

            ALTER TABLE `film_times`
            ADD CONSTRAINT `fk_film_times_films1`
              FOREIGN KEY (`film_id`)
              REFERENCES `films` (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            ALTER TABLE `films`
            ADD CONSTRAINT `fk_films_rooms`
              FOREIGN KEY (`room_id`)
              REFERENCES `rooms` (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        ");

        echo __CLASS__." migrated.\n";
    }

    public function down()
    {
        $this->execute("
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

            ALTER TABLE `kino`.`bookings`
            DROP FOREIGN KEY `fk_bookings_films1`,
            DROP FOREIGN KEY `fk_bookings_film_times1`;

            ALTER TABLE `kino`.`film_times`
            DROP FOREIGN KEY `fk_film_times_films1`;

            ALTER TABLE `kino`.`films`
            DROP FOREIGN KEY `fk_films_rooms`;

            ALTER TABLE `kino`.`bookings`
            DROP COLUMN `film_id`,
            DROP INDEX `fk_bookings_film_times1_idx` ,
            DROP INDEX `fk_bookings_films1_idx` ;

            ALTER TABLE `kino`.`film_times`
            DROP COLUMN `film_id`,
            DROP INDEX `fk_film_times_films1_idx` ;

            ALTER TABLE `kino`.`films`
            DROP COLUMN `room_id`,
            DROP INDEX `fk_films_rooms_idx` ;

            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        ");

        echo __CLASS__." reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
