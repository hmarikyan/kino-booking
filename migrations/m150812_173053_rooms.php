<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_173053_rooms extends Migration
{
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `rooms`");

        $this->execute("CREATE TABLE `rooms` (
          `id` INT NOT NULL AUTO_INCREMENT COMMENT 'primary key',
          `name` VARCHAR(50) NULL COMMENT '',
          `param` VARCHAR(50) NULL COMMENT 'color parameter for making comparisons',
          PRIMARY KEY (`id`)  COMMENT '')
          ");

        $this->execute("
          INSERT INTO `rooms` (`id`, `name`, `param`) VALUES
            (1, 'Special Room', 'special'),
            (2, 'Standard Room', 'standard'),
            (3, 'Lux 7D', 'lux_7d');
        ");

        echo __CLASS__." migrated.\n";
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `rooms`");

        echo __CLASS__." reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
