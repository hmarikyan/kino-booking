<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_175739_film_times extends Migration
{
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `film_times`");

        $this->execute(" CREATE TABLE `film_times` (
              `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
              `film_id` INT NULL COMMENT 'foreign key reference to films table',
              `time` DATETIME NULL COMMENT 'time of showing the film',
              PRIMARY KEY (`id`)  COMMENT '')");

        $this->execute("
          INSERT INTO `film_times` (`id`, `film_id`, `time`) VALUES
            (7, 1, '2015-08-13 06:10:19'),
            (8, 2, '2015-08-18 05:05:50'),
            (9, 1, '2015-08-13 10:10:57'),
            (10, 5, '2015-08-20 03:00:20'),
            (11, 5, '2015-08-18 12:00:43'),
            (12, 3, '2015-08-19 02:00:35'),
            (13, 3, '2015-08-26 04:00:39'),
            (14, 6, '2015-08-19 18:30:46'),
            (15, 6, '2015-08-25 09:25:16');
        ");

        echo __CLASS__." migrated.\n";
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `film_times`");

        echo __CLASS__." reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
