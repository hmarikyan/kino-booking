<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_181349_bookings extends Migration
{
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `bookings`");

        $this->execute("CREATE TABLE `bookings` (
                  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
                  `film_id` INT NULL COMMENT 'foreign key - ref to films',
                  `film_time_id` INT NULL COMMENT 'foreign key - ref to film_times',
                  `row` INT NULL COMMENT 'row of room',
                  `column` INT NULL COMMENT 'column of room',
                  `booker` VARCHAR(150) NULL COMMENT 'booker name',
                  PRIMARY KEY (`id`)  COMMENT '')
              ");

        echo __CLASS__." migrated.\n";
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `bookings`");

        echo __CLASS__." reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
