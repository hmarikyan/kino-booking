function Booking(){
    var that = this;

    /*seat object -> send by ajax for saving*/
    this.reserve = {
        film_id : null,
        time_id : null,
        booker : null,
        row : null,
        col : null
    };

    /*common event listeners*/
    this.addEvents = function(){
        jQuery(document).ready(function(){
            /* dropdown Menu 3rd level*/
            jQuery(".navbar-nav .dropdown-submenu:not(.select_time)>a").mouseenter(function(e){
                jQuery(".dropdown-submenu .dropdown-menu").hide();

                jQuery(this).find("~ul.dropdown-menu").show();
            });

            /*listener -> select film and time*/
            jQuery(".navbar-nav .select_time").on("click",that.selectFilmTime);

            /*listener -> book a seat*/
            jQuery(".btn_book").on("click",that.book);

        })
    };

    /*handler -> get bookings for certain film/time*/
    this.selectFilmTime = function(){
        var f = jQuery(this);
        that.reserve.film_id = f.data("film");
        that.reserve.time_id = f.data("time");

        /*Set film/time to the Title*/
        var film = jQuery(this).parent(".dropdown-menu").siblings("a").text();
        var room = jQuery(this).parent(".dropdown-menu").parents(".dropdown-menu").siblings("a").text();
        var time = jQuery(this).find("a").text();
        jQuery("#film_time").text(" - "+ room+ " - "+film+ " - "+ time);

        jQuery.ajax({
            url: '/site/getbookings',
            type: 'GET',
            data: {
                film_id: that.reserve.film_id,
                time_id: that.reserve.time_id
            },
            success: function(data) {
                that.resetSeats();

                if(data.length <= 0) return false;

                /*disable booked buttons*/
                for(var i = 0 ; i < data.length ; i++){
                    that.disableSeat(data[i]);
                }
            },
            error: function() {
               console.log(arguments);
            }
        })
    };

    /*handler -> book a seat*/
    this.book = function(){
        var b = jQuery(this);
        that.reserve.row = b.data("row");
        that.reserve.col = b.data("col");

        booker_popup.dialog( "open" );
    };

    /*this is being called after dialog successfully closing*/
    this.bookingSave = function(){
        /*require -> film + seat + booker*/
        if(that.reserve.time_id == null || that.reserve.row == null){
            alert("Please Select Film, Time and Seat!"); return;
        }

        jQuery.ajax({
            url: '/site/savebookings',
            type: 'POST',
            data: that.reserve,
            success: function(data) {
               if(typeof data !== "undefined")
                  that.disableSeat(data);
               else
                 console.log(data);
            },
            error: function() {
                alert("Error booking the seat!");
                console.log(arguments);
            }
        })

    };

    /*Reset all btn's after reselect a film*/
    this.resetSeats = function(){
        jQuery(".sits-outer").slideDown();
        jQuery(".btn_book").prop("disabled",false);

        var btns = jQuery(".btn_book");
        btns.each(function(k,v){
            var btn = jQuery(v);
            btn.html(btn.data("row")+" / "+ btn.data("col"));
        })
    };

    /*disable booked sbtns/seats*/
    this.disableSeat = function(data){
        var btn = jQuery("#"+data.row+"_"+data.column);
        btn.prop("disabled","disabled");
        btn.html(data.booker);
    };

    /* DIALOG WINDOW - jui*/
    var booker_popup = jQuery( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            cancel: function() {
                booker_popup.dialog("close");
            },
            book: function() {
                var booker = jQuery("#booker").val();

                /*Validation*/
                if(booker.length > 20 || booker.length < 3){
                    jQuery("#dialog_error").html("The name should be between 3 and 20 characters");
                    return false;
                }
                that.reserve.booker = booker;
                booker_popup.dialog("close");

                that.bookingSave();
            }
        },
        close: function() {
            jQuery("#booker").val("");
            jQuery("#dialog_error").html("");
        }
    });

}

var b = new Booking();

/*set event listeners*/
b.addEvents();
