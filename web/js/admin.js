function Admin(){
    var that = this;

    this.addEvents = function(){
        jQuery(document).ready(function(){

            /*Timepicker - > films create/edit page*/
            jQuery(".add_time").on("click",that.add_time);
            jQuery('.datetimepicker').datetimepicker({"startView":2,"minView":0,"maxView":1,"autoclose":true,"format":"yyyy-mm-dd hh:ii:ss","todayBtn":true,"language":"en"})
        })
    }

    /*handler -> add time field/text input for film time record*/
    this.add_time = function(e){
        var id = jQuery(this).data("id") + 1;

        var html = '<div class="form-group field-filmtimes-time">' +
                '<input type="text" class="form-control datetimepicker_'+id+' input-ms" name="FilmTimes['+id+'][time]" readonly="readonly"> <div class="help-block"></div>'+
            '</div>';

        jQuery(".times").append(html);
        jQuery('.datetimepicker_'+id).datetimepicker({"startView":2,"minView":0,"maxView":1,"autoclose":true,"format":"yyyy-mm-dd hh:ii:ss","todayBtn":true,"language":"en"});

        jQuery(this).data("id", id);
    }
}

var admin = new Admin();

/*attach*/
admin.addEvents();
