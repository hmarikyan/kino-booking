<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Rooms;
use app\models\Bookings;
use app\models\Films;
use app\models\FilmTimes;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*MAIN PAGE -> BOOKING*/
    public function actionIndex()
    {
        $rooms = Rooms::find()->all();

        return $this->render('index',array(
            'rooms'=>$rooms
        ));
    }

    /*LOGIN -> to admin panel*/
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect("/film/index");
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /*Get Booking by ajax request*/
    public function actionGetbookings(){

        if(!Yii::$app->request->isAjax) return false;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $bookings = Bookings::find()
                ->where(array(
                    "film_id"=>Yii::$app->request->get("film_id"),
                    "film_time_id"=>Yii::$app->request->get("time_id")
                ))->all();

        return $bookings;
    }

    /*Save Booking by ajax request*/
    public function actionSavebookings(){

        if(!Yii::$app->request->isAjax) return false;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $booking = new Bookings();
        $booking->film_id = Yii::$app->request->post("film_id");
        $booking->film_time_id = Yii::$app->request->post("time_id");
        $booking->row = Yii::$app->request->post("row");
        $booking->column = Yii::$app->request->post("col");
        $booking->booker = Yii::$app->request->post("booker");
        $booking->save();

        return $booking;
    }
}
