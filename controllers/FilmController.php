<?php

namespace app\controllers;

use Yii;
use app\models\Films;
use app\models\FilmTimes;
use app\models\Rooms;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * FilmController implements the CRUD actions for Films model.
 */
class FilmController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];

    }

    public function init(){
        $this->layout = "admin";
    }

    /**
     * Lists all Films models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Films::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Films model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Films model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Films();

        $modelTimes = array(new FilmTimes());

        $rooms = Rooms::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $PostFilmTimes = Yii::$app->request->post("FilmTimes");
            if(!empty($PostFilmTimes)) {
                foreach($PostFilmTimes as $time){
                    if(!empty($time['id'])){
                        $timeModel = FilmTimes::find()->where(array("id"=>$time['id']))->one();
                    }else{
                        $timeModel = new FilmTimes();
                    }

                    $timeModel->film_id = $model->id;
                    $timeModel->time = $time['time'];
                    $timeModel->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelTimes' => $modelTimes,
                'rooms' => $rooms,
            ]);
        }
    }

    /**
     * Updates an existing Films model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rooms = Rooms::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $PostFilmTimes = Yii::$app->request->post("FilmTimes");
            if(!empty($PostFilmTimes)) {
                foreach($PostFilmTimes as $time){
                    if(!empty($time['id'])){
                        $timeModel = FilmTimes::find()->where(array("id"=>$time['id']))->one();
                    }else{
                        $timeModel = new FilmTimes();
                    }

                    $timeModel->film_id = $model->id;
                    $timeModel->time = $time['time'];
                    $timeModel->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelTimes' => $model->filmTimes,
                'rooms' => $rooms
            ]);
        }
    }

    /**
     * Deletes an existing Films model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Films model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Films the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Films::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
