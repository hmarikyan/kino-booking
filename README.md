Project Installation.

Below I present "Kino" project setup on xampp/windows. on other platforms setup steps are almost the same.
Considering you already have apache web server installed with mysql server.

The project is written by PHP/Yii2 framework and it's on the bit-bucket vcs.

PS. Feel free to contact to me , when/if there is any issues during installation.

--------------------------------------------------------------------------------
1. Go to Git bash.
(download link "https://git-scm.com/downloads")
 
 Make sure you have also composer - php dependency manager.
(download link "https://getcomposer.org/doc/00-intro.md")

-------------------------------------------------------------------------------------------
2. 
a. In Git bash - go to server webroot directory - "cd /c/xampp/htdocs"
Type "git clone https://hovogegham@bitbucket.org/hovogegham/stdev-kino.git" 

b. After downloading the project you need to get dependencies e.g. Yii2 core/libraries.
In git bash go to project directory - "cd stdev-kino",
then type "composer install"
		
------------------------------------------------------------------------------------------------------------------------		
3. 
a. Go to http://localhost/phpmyadmin or your preferred application working with mysql server. 
Create new datebase - "kino" 

b.in project directory - open /config/db.php  and setup your database host,username,password,db

c. In Git bash - go to project root directory, type - "yii migrate". it will create needed db-tables and set indexes/foreign keys

------------------------------------------------------------------------------------------------------------------------------------------

4. Create apache v-host.
Open "C:\xampp\apache\conf\extra\httpd-vhosts.conf" 
and add this code. As well as you can modify it for you preferred servername/project directory
----
<VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs/stdev-kino/web"
    ServerName kino.loc
    ErrorLog "logs/kino.log"
</VirtualHost>
-----

-----------------------------------------------------------------------
5. Open "C://windows/system32/drivers/etc/hosts" and 
add this row "127.0.0.1       kino.loc"

--------------------------------------------------------------------------
6. Restart Apache server.

--------------------------------------------------------------------------
7. In browser type "http://kino.loc"
There you can the application - booking system.

a. Click on some room (ex. Special room), then select film name of opened menu, then CLICK on some film time.
It will automatically open seats/visual room. You can click on some seat, type your name and it will save your seat/booking.
Further nobody is able to book your seat again.  

b. If you reload the page and open that film/time it will show your already booked seat.

c. In order to open admin. panel - in header menu, at right, click login, type admin/admin then login and it will take you to admin panel.
There is ability to manage(create/update/delete) rooms, films and film times.

-------------------------------------------------------------------------------------------------------------------  

Good luck. 
Thanks.